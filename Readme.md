# Repo Instafeed Gallery
You can use this repo for free, bro!

- Instafeed official website:
[Instafeed](http://instafeedjs.com/)

- For generate Instagram user id:
[Codeofaninja](https://codeofaninja.com/tools/find-instagram-user-id)

- For generate Access Token:
[Pixelunion](https://instagram.pixelunion.net)

### Basic Code:
File index.html:
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Instafeed Gallery</title>
    <link rel="stylesheet" href="./instapp.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    
    <div class="jumbotron">
        <div class="container">
            <h4>Arisid Gallery</h4>
        </div>
    </div>
    <div class="container">
        <div class="row" id="instafeed"></div>
    </div>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.1/instafeed.min.js'></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./instapp.js"></script>
</body>
</html>
```
File instapp.js
```
let galleryFeed = new Instafeed({
    get: 'user',
    userId: [USER ID(string)],
    accessToken: [ACCESS TOKEN(string)],
    template: '<div class="col-lg-4 mb-4"><a href="{{link}}"><img class="img-reponsive" src="{{image}}"/></a></div>',
    resolution: 'standard_resolution',
    limit: 10
});
galleryFeed.run();

```
File instapp.css
```
img.img-reponsive{
    width: 100%;
    height: 350px;
    object-fit: cover;
}
```
### Result
![alt](result.png)

## Selamat Mencoba Broh!